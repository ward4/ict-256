$(function () {

    $.validator.addMethod("PWCHECK",
        function(value,element){
        if(/^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&]){1,}).{8,}$/.test(value)) {
            return true ;
            }else {
                    return false ;
                };
        });

    $("#inscription_form").validate(
       {
           rules: {
               nom_per: {
                   required: true,
                   minlength: "2" ,
               },
               nom_deu: {
                   required: true,
                   minlength: "2" ,
               },
               Email:{
                    required: true,
                    email: true,
               },
               motdepasse:{
                   required: true,
                   minlength: 5,
                   PWCHECK: true,
               },
               confirmentmotdepasse:{
                   required: true,
                   equalTo: "#motdepasse"
               }

           },


           messages: {

               nom_per:{
                   required:"Veuillez saisir votre prenom nom",
                   minlength: "trop court : Votre nom doit être composé de 2 caractères au minimum.",
               },
               nom_deu:{
                   required:"Veuillez saisir votre nom",
                   minlength: "trop court : Votre nom doit être composé de 2 caractères au minimum.",
               },
               Email: {
                   required:"Veuillez saisir votre E-mail",
                  email : "Votre adresse e-mail doit avoir le format suivant : name@domain.com",
               },
               motdepasse:{
                   required:"Veuillez saisir votre mot de passe ",
                   minlength: "Votre mot de passe est trop court",
                   PWCHECK: "Le mot de passe doit comporter au minimum 8 caractéres, dont une minuscule, une majuscule, un chiffre et un caractére spécial."
               },
               confirmentmotdepasse:{
                   required:"Les mots de passe ne sont pas identiques",
                   equalTo: "Veuillez saisir une deuxième fois votre mot de passe"
               }
           }
       })
});