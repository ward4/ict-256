var password = document.getElementById("mot_de_passe")
    , confirm_password = document.getElementById("confirment_mot_de_passe");

function validatePassword(){
    if(password.value != confirm_password.value) {
        confirment_mot_de_passe.setCustomValidity("mot de passe n'est pas les même");
    } else {
        confirm_password.setCustomValidity('');
    }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

