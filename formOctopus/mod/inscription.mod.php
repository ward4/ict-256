<!-- Modal -->
<div id="inscriptionModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>

            <div class="modal-body">
                <form id="inscription_form" action="" method="">

                    <!-- Moniteur -->
                    <div class="form-group row">
                        <div class="col-sm-offset-1 checkbox">
                            <label class="col-sm-2 col-form-label">
                                <input type="checkbox" value="0" id="moniteur" name="moniteur">Moniteur
                            </label>
                            <span class="col-sm-2">
                                <input type="number" class="form-control" id="NB_eleves" min="1" max="14" placeholder="NB">
                            </span>

                        </div>
                    </div>
                    <div class="row col-sm-offset-1">
                        <div class="col-md-4">
                            <h4>Catégorie</h4>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="categorie" id="plongeur"
                                       value="option1">
                                <label class="form-check-label" for="plongeur">Plongeur</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="categorie" id="apneiste"
                                       value="option2">
                                <label class="form-check-label" for="apneiste">Apnéiste</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="categorie" id="nageur"
                                       value="option3">
                                <label class="form-check-label" for="nageur">Nageur</label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h4>Type</h4>
                            <div class="row">
                                <div class="form-check col-md-6">
                                    <input class="form-check-input col-md-3 club" type="radio" name="type"
                                           value="0">
                                    <label class="form-check-label col-md-9" for="club">Ecole / Club</label>

                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control col-md-2" id="club_text" name="club_text"
                                           placeholder="Ecole / Club">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-check col-md-6">
                                    <input class="form-check-input col-md-3 club" type="radio" name="type"
                                           value="1">
                                    <label class="form-check-label col-md-9" for="prive">Privé</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="prenom_per" class="col-sm-offset-1 col-sm-2 col-form-label">Prénom</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="prenom_per" name="prenom_per"
                                       placeholder="Votre prénom">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="nom_per" class="col-sm-offset-1 col-sm-2 col-form-label">Nom</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="nom_per" name="nom_per"
                                       placeholder="Votre nom">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="email_per" class="col-sm-offset-1 col-sm-2 col-form-label">E-mail</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email_per" name="email_per"
                                       placeholder="Votre adresse email">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="telephone" class="col-sm-offset-1 col-sm-2 col-form-label">Téléphone</label>
                            <div class="col-sm-9">
                                <input type="tel" class="form-control" id="telephone" name="telephone"
                                       placeholder="+41 00 000 00 00">
                            </div>
                        </div>
                    </div>

                    <!-- Btn conf et annuler -->
                    <div class="form-group row">
                        <div class="col-sm-offset-8 col-sm-2">
                            <input type="submit" class="form-control btn btn-primary submit" id="submit-conf"
                                   value="S'inscrire">
                        </div>
                        <div class="col-sm-2">
                            <input type="reset" class="form-control btn btn-warning" id="reset-conf"
                                   value="Annuler">
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
