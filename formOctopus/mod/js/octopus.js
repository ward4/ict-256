$(function () {
    $("#NB_eleves").hide();
    $("#club_text").hide();

    $.validator.addMethod("TELCHECK",
        function (value, element) {
            return /^(\+?)(\d{2,4})(\s?)(\-?)((\(0\))?)(\s?)(\d{2})(\s?)(\-?)(\d{3})(\s?)(\-?)(\d{2})(\s?)(\-?)(\d{2})/.test(value)
        }
    );

    $("#moniteur").on("click", function () {
        if ($("#moniteur").prop("checked")) {
            console.log("Checked");
            $("#NB_eleves").show();
        } else {
            console.log("Unchecked");
            $("#NB_eleves").hide();
        }
    });

    $("[name=type]").change(function () {
        if ($("[name=type]:checked").val() == 0) {
            console.log("Selected");
            $("#club_text").show();
        } else {
            console.log("Unselected");
            $("#club_text").hide();
        }
    });

    $("#reset-conf").on("click", function () {
        $("#NB_eleves").hide();
        $("#club_text").hide();
    });


    $("#inscription_form").validate({
        debug: true,
        rules: {
            nom_per: {
                required: true,
                minlength: 2
            },

            prenom_per: {
                required: true,
                minlength: 2
            },

            email_per: {
                required: true,
                email: true
            },

            telephone: {
                required: true,
                TELCHECK: true
            },

            categorie: {
                required: true
            },

            NB_eleves: {
                required: true
            }
        },

        messages: {
            nom_per: {
                minlength: "Votre nom doit être composé de 2 caractères au minimum.",
                required: "Veuillez saisir votre nom."
            },

            prenom_per: {
                minlength: "Votre prénom doit être composé de 2 caractères au minimum.",
                required: "Veuillez saisir votre prénom."
            },

            email_per: {
                required: "Veuillez saisir votre email.",
                email: "Votre adresse email doit avoir le format suivant : name@domain.com."
            },

            telephone: {
                required: "Veuillez donner un tel",
                TELCHECK: "Respectez tel"
            },

            categorie: {
                required: "pas content"
            },

            NB_eleves: {
                required: "pls"
            }
        }
    });
});