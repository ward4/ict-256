$("p:first").css ("margin-top", "50");


$(".color").css("background", "#64C5AB");
$(".color").css("border-color", "red");

$("p:not(.color)").css("background", "red");
$("p:not(.color)").css("border-color", "#64C5AB");

$("body").on("click", function () {
    $(".color").fadeToggle(10000);
    $("p:not(.color)").slideToggle(10000);
})